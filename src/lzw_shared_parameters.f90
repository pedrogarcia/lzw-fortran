!
! lzw_shared_parameters.f90
!
! LZW Common Variables Used by Coder and Decoder
!
! Author: Pedro Garcia Freitas <sawp@sawp.com.br>
! May, 2011
!
! License: Creative Commons http://creativecommons.org/licenses/by-nc-nd/3.0/
!
module LZW_Shared_Parameters
  implicit none
  ! change this if compiler dont use 32 bits for integer
  integer, parameter :: COMPILER_INTEGER_SIZE = 32 
  integer, parameter :: BITS = 12
  integer, parameter :: FILEIN = 66
  integer, parameter :: FILEOUT = 99
  integer, parameter :: MAX_VALUE = (2 ** BITS) - 1
  integer, parameter :: MAX_CODE = MAX_VALUE - 1
  integer, parameter :: MAX_DICTIONARY_SIZE = 5021
  integer, parameter :: SYMBOL_SIZE = 8
  integer, parameter :: MISSING_BITS = COMPILER_INTEGER_SIZE - SYMBOL_SIZE
  integer            :: status  

  integer, dimension(0:MAX_DICTIONARY_SIZE) :: prefixCodes
  integer, dimension(0:MAX_DICTIONARY_SIZE) :: concatenatedSymbols
end module LZW_Shared_Parameters
