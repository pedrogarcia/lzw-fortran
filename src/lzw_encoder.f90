!
! lzw_encoder.f90
!
! LZW Coder (Compressor)
!
! Author: Pedro Garcia Freitas <sawp@sawp.com.br>
! May, 2011
!
! License: Creative Commons http://creativecommons.org/licenses/by-nc-nd/3.0/
!
module LZW_Encoder
  use LZW_Shared_Parameters
  use codecIO
  implicit none

  integer, parameter                        :: HASH_SHIFT = BITS - SYMBOL_SIZE
  integer, dimension(0:MAX_DICTIONARY_SIZE) :: symbolValues

  contains
    subroutine compress()
      integer   :: symbol
      integer   :: codedSymbol
      integer   :: index
      integer   :: nextSymbol

      nextSymbol = COMPILER_INTEGER_SIZE * SYMBOL_SIZE
      symbolValues(:) = -1

      codedSymbol = getRawByte()
      do
        symbol = getRawByte()
        if (status /= 0) then
          exit
        endif

        index = getPositionOnDictionary(codedSymbol, symbol)
        if (symbolValues(index) /= -1) then
            codedSymbol = symbolValues(index)
        else
          if (nextSymbol <= MAX_CODE) then
            symbolValues(index) = nextSymbol
            nextSymbol = nextSymbol + 1
            prefixCodes(index) = codedSymbol
            concatenatedSymbols(index) = symbol
          endif
          call setOutputCode(codedSymbol)
          codedSymbol = symbol
        endif
      end do
      call setOutputCode(codedSymbol)
      call setOutputCode(MAX_VALUE)
      call setOutputCode(0)
    end subroutine


    function getPositionOnDictionary(hashPrefix, hashSymbol)
      integer, intent(in) :: hashPrefix
      integer, intent(in) :: hashSymbol
      integer             :: getPositionOnDictionary
      integer             :: index
      integer             :: offset

      index = ishft(hashSymbol, HASH_SHIFT)
      index = ieor(index, hashPrefix)
      if (index == 0) then
        offset = 1
      else
        offset = MAX_DICTIONARY_SIZE - index
      endif

      do
        if (symbolValues(index) == -1) then
          getPositionOnDictionary = index
          exit
        endif

        if (prefixCodes(index) == hashPrefix .and. &
            & concatenatedSymbols(index) == hashSymbol) then
          getPositionOnDictionary = index
          exit
        endif

        index = index - offset
        if (index < 0) then
          index = index + MAX_DICTIONARY_SIZE
        endif
      end do
    end function
end module LZW_Encoder
