!
! lzw_decoder.f90
!
! LZW Decoder (Expanssor)
!
! Author: Pedro Garcia Freitas <sawp@sawp.com.br>
! May, 2011
!
! License: Creative Commons http://creativecommons.org/licenses/by-nc-nd/3.0/
!
module LZW_Decoder
  use LZW_Shared_Parameters
  use codecIO
  implicit none

  type DECODE_BUFFER_STACK
    integer, dimension(0:MAX_DICTIONARY_SIZE) :: decoderStack
    integer                                   :: top
  end type DECODE_BUFFER_STACK

  type(DECODE_BUFFER_STACK) :: stack

  contains
    subroutine decompress()
      integer            :: nextSymbol
      integer            :: newSymbol
      integer            :: oldSymbol
      integer            :: symbol
      integer            :: popedSymbol

      nextSymbol = COMPILER_INTEGER_SIZE * SYMBOL_SIZE
      oldSymbol = getInputCode()
      symbol = oldSymbol

      call setRawByte(oldSymbol)
      
      do
        newSymbol = getInputCode()

        if (newSymbol == MAX_VALUE) then
          stop
        endif

        if (newSymbol >= nextSymbol) then
          stack%decoderStack(0) = symbol
          call decodeSymbol(stack%decoderStack(1:), oldSymbol)
        else
          call decodeSymbol(stack%decoderStack(:), newSymbol)
        endif

        symbol = stack%decoderStack(stack%top)

        do while(stack%top >= 0)
          popedSymbol = stack%decoderStack(stack%top)
          call setRawByte(popedSymbol)
          stack%top = stack%top - 1
        end do

        if (nextSymbol <= MAX_CODE) then
          prefixCodes(nextSymbol) = oldSymbol
          concatenatedSymbols(nextSymbol) = symbol
          nextSymbol = nextSymbol + 1
        endif
        oldSymbol = newSymbol
      end do
    end subroutine
    

    subroutine decodeSymbol(buffer, code)
      integer, intent(in)  :: code
      integer              :: symbol
      integer              :: j, i
      integer, dimension(:), intent(inout) :: buffer

      j = 0
      symbol = code
      stack%top = 0
      do
        if (symbol < COMPILER_INTEGER_SIZE * SYMBOL_SIZE) then
          exit
        endif

        if (j >= MAX_CODE) then
          print *, "Decoding error"
          stop
        endif

        i = stack%top + 1
        buffer(i) = concatenatedSymbols(symbol)
        symbol = prefixCodes(symbol)
        stack%top = stack%top + 1
        j = j + 1
      end do
      i = j + 1
      buffer(i) = symbol
    end subroutine decodeSymbol 
end module LZW_Decoder
