!
! lzw.f90
!
! LZW Coder and Decoder
!
! Author: Pedro Garcia Freitas <sawp@sawp.com.br>
! May, 2011
!
! License: Creative Commons http://creativecommons.org/licenses/by-nc-nd/3.0/
!

module LZW
  use LZW_Shared_Parameters
  use LZW_Encoder
  use LZW_Decoder
  implicit none

  contains
    subroutine init(input, output, operation, fileName)
      character(len=100), intent(in) :: input
      character(len=100), intent(in) :: output
      character(len=1)   :: operation
      character(len=100) :: fileName

      if (operation /= 'd' .and. operation /= 'e') then
        print *, "Usage: " // trim(fileName) // " <operation> input output"
        print *, "Possible operations: "
        print *, "    e -> encode (compress)"
        print *, "    d -> decode (inflate)"
        stop
      endif

      open(unit=FILEIN, file=input, action="read", status="old", access='stream', form="formatted")
      open(unit=FILEOUT, file=output, action="write", status="replace", &
           access='stream', form="formatted")

      if (operation == 'd') then
        print *, "Decoding..."
        call decompress()
      else
        print *, "Coding..."
        call compress()
      endif

      close(unit=FILEIN)
      close(unit=FILEOUT)
    end subroutine init  
end module LZW

program main
  use LZW

  character(len=100) :: input
  character(len=100) :: output
  character(len=1)   :: operation
  character(len=100) :: fileName
  
  call getarg(0, fileName)
  call getarg(1, operation)
  call getarg(2, input)
  call getarg(3, output)
  call init(input, output, operation, fileName)
end program main
