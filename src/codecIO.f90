!
! codecIO.f90
!
! bit IO routines for coder and encoder.
!
! Author: Pedro Garcia Freitas <sawp@sawp.com.br>
! May, 2011
!
! License: Creative Commons http://creativecommons.org/licenses/by-nc-nd/3.0/
!
module codecIO
  use LZW_Shared_Parameters
  implicit none

  contains
    subroutine setOutputCode(code)
      integer, intent(in) :: code
      integer             :: shiftedSymbol
      integer             :: buffer
      integer             :: shift
      integer, save       :: outputBitCount = 0
      integer, save       :: outputBitBuffer = 0

      shift = COMPILER_INTEGER_SIZE - BITS - outputBitCount
      shiftedSymbol = ishft(code, shift)
      outputBitBuffer = ior(outputBitBuffer, shiftedSymbol)
      outputBitCount = outputBitCount + BITS

      do
        if (outputBitCount < SYMBOL_SIZE) then
          exit
        endif
        buffer = ishft(outputBitBuffer, -MISSING_BITS)
        call setRawByte(buffer)
        outputBitBuffer = ishft(outputBitBuffer, SYMBOL_SIZE)
        outputBitCount = outputBitCount - SYMBOL_SIZE
      end do
    end subroutine setOutputCode

    
    subroutine setRawByte(symbol)
      integer   :: symbol

      call fputc(FILEOUT, achar(symbol))
    end subroutine setRawByte


    function getRawByte()
      integer   :: getRawByte
      character :: bufferedByte

      call fgetc(FILEIN, bufferedByte, status)
      getRawByte = iachar(bufferedByte)
    end function

    function getInputCode()
      integer         :: getInputCode
      integer         :: returnn
      integer         :: shiftedBit
      integer         :: integerInputBuff
      integer, save   :: inputBitCounter = 0
      integer, save   :: inputBitBuffer = 0

      do
        if (inputBitCounter > MISSING_BITS) then
          exit
        endif

        integerInputBuff = getRawByte()
        shiftedBit = ishft(integerInputBuff, MISSING_BITS - inputBitCounter)
        inputBitBuffer = ior(inputBitBuffer, shiftedBit)
        inputBitCounter = inputBitCounter + SYMBOL_SIZE
      end do

      returnn = ishft(inputBitBuffer, BITS - COMPILER_INTEGER_SIZE)
      inputBitBuffer = ishft(inputBitBuffer, BITS)
      inputBitCounter = inputBitCounter - BITS
      getInputCode = returnn
    end function getInputCode
end module codecIO
