LZW
===

Implementation of Lempel-Ziv-Welch algorithm in Fortran.


Description
-----------

I developed this program for a college homework. This program is able to
compress and decompress any file using the
[Lempel–Ziv–Welch (LZW) algorithm][1].


Installation
------------

1. Clone the source code
> `git clone https://gitlab.com/pedrogarcia/lzw-fortran`
2. Build the source
> `cd src`  
> `make`


Execution
---------

To compress the **raw.txt** file, saving as the compressed file **coded.lzw**,
proceed as follows

> `./lzw e raw.txt coded.lzw`

To recover/decompress the original data in **raw.txt** file, use the `d` option

> `./lzw d coded.lzw raw_recovered.txt`


License
-------

This project is released under [GNU GPL version 2][2].



[1]: http://en.wikipedia.org/wiki/Lempel–Ziv–Welch
[2]: http://www.gnu.org/licenses/gpl-2.0.html
